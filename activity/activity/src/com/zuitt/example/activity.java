
package com.zuitt.example;
import java.util.Scanner;

// Activity: Instructions
// 1. Create a new Java project named activity.
// 2. Declare the following variables to store the user's information
    // 2.a firstName - String
    // 2.b lastName - String
    // 2.c firstSubject - Double
    // 2.d secondSubject - Double
    // 2.e thirdSubject - Double
// 3. Instantiate/Create a new Scanner Object.

public class activity {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter your first name:");
        String firstName = (scan.nextLine());
        System.out.println("Please enter your last name:");
        String lastName = scan.nextLine();

        System.out.println("Please enter your grade for your first subject:");
        Double firstSubject = Double.parseDouble(scan.nextLine());
        System.out.println("Please enter your grade for your second subject:");
        Double secondSubject = Double.parseDouble(scan.nextLine());
        System.out.println("Please enter your grade for your third subject:");
        Double thirdSubject = Double.parseDouble(scan.nextLine());
        System.out.println("Hello " + firstName + " " + lastName + "! Your grades are: " + firstSubject + ", " + secondSubject + ", " + thirdSubject);
    }
}
